/*
MIT License

Copyright (c) 2018 Joao Macedo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef nc_proj
#define nc_proj

#include <iostream>
#include "netcdfdataset.h"
#include "gdalwarper.h"

/* Override netcdf library functions: */
typedef int nc_type;

const char *nc_strerror(int ncerr);

int nc_put_att_double(int ncid, int varid, const char *name,
                      nc_type xtype, size_t len, const double *op);

int nc_put_att_text(int ncid, int varid, const char *name,
                    size_t len, const char *op);

int nc_def_var(int ncid, const char *name, nc_type xtype, int ndims,
               const int *dimidsp, int *varidp);


/* Get netcdf grid_mapping (string and double)
   attributes, from PROJ string:
*/
std::pair< std::map<std::string, std::string>,
           std::map<std::string, double> > get_grid_mapping_attrs(const char *proj_str);


std::pair< std::string,
           std::map<std::string, std::string> > get_xcoord_attrs(const char *proj_str);


std::pair< std::string,
           std::map<std::string, std::string> > get_ycoord_attrs(const char *proj_str);


std::pair< double*, double* > get_coord_values(double adfGeoTransform[6],
                                               size_t nRasterXSize,
                                               size_t nRasterYSize,
                                               bool bBottomUp=false);

std::pair< double*, long* > get_suggested_warp_output(double srcGeoTransform[6],
                                                     long srcShape[2],
                                                     const char *src_wkt,
                                                     const char *dst_wkt);

#endif
