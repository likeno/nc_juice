#!/usr/bin/env python
#-*- coding:utf-8 -*-

import os
import logging
from itertools import product
from collections import OrderedDict
from functools import reduce
from osgeo import gdal, osr, gdal_array
import numpy as np
import xarray as xr
from cfgrib import FileStream
import uuid
import datetime as dt
from netCDF4 import Dataset

from . import nc_proj

logger = logging.getLogger(__name__)
gdal.UseExceptions()


def process_nc_datasets(in_paths, netcdf_path, 
                        src_proj_str=None, src_geot=None,
                        trg_proj_str=None, trg_wkt=None,
                        trg_geot=None, trg_shape=None,
                        dataset_list=[], sel_dict=None, isel_dict=None,
                        common_encoding={'zlib': True, 'complevel': 9},
                        attrs={'Conventions': 'CF-1.5'}):

    if isinstance(in_paths, str) or isinstance(in_paths, bytes):
        in_paths = [in_paths]

    src_xdset = open_nc_dataset(in_paths, dataset_list, sel_dict, isel_dict)

    if src_proj_str is None or src_geot is None:
        (src_proj_str, src_geot,
         src_shape) = get_georef_from_file(in_paths[0])
        logger.debug(f"\src_proj_str: {src_proj_str}")
    if src_xdset is not None:
        reproj_xdset = reproject_dataset(src_xdset, src_proj_str, src_geot,
                                         trg_proj_str, trg_wkt, trg_geot,
                                         trg_shape)

        reproj_xdset.attrs.update(attrs)
        reproj_xdset.to_netcdf(
            netcdf_path, encoding={xvar.name:{'zlib': True, 'complevel': 9}
                                   for xvar in reproj_xdset.data_vars.values()}
        )

        logger.info(f"\tWrite netcdf file successfuly: {netcdf_path}")


def open_nc_dataset(in_paths, dataset_list=[], sel_dict=None, isel_dict=None):
    if isinstance(in_paths, str) or isinstance(in_paths, bytes):
        in_paths = [in_paths]

    xdset_list = []
    # Merge datasets from paths list
    for idx, path in enumerate(in_paths):
        logger.info("\tProcessing netCDF file: "
                    "{}".format(path))
        xdset = xr.open_dataset(path, cache=False, decode_cf=False)
        # Get datasets subset and/or select by lables/index
        if any(dataset_list) or any((sel_dict, isel_dict)):
            sel_xvar_list = []
            for var_name in list(xdset.data_vars):
                if not any(dataset_list) or var_name in dataset_list:
                    logger.info("\tdataset: "
                                "{}".format(var_name))
                    xvar = xdset.get(var_name)
                    if sel_dict is not None:
                        logger.debug("\txarray.DataArray label selection: {}"
                                     "".format(sel_dict))
                        xvar = xvar.sel(**sel_dict)
                    if isel_dict is not None:
                        logger.debug("\txarray.DataArray index "
                                     "selection: {}".format(isel_dict))
                        xvar = xvar.isel(**isel_dict)
                    sel_xvar_list.append(xvar)
            xdset = xr.merge(sel_xvar_list) if len(sel_xvar_list) != 0 else None
        if xdset is not None:
            xdset_list.append(xdset)
    if len(xdset_list) != 0:
        return xr.merge(xdset_list)


def process_grib_dataset(grib_path, netcdf_path, 
                         src_proj_str=None, src_geot=None,
                         trg_proj_str=None, trg_wkt=None,
                         trg_geot=None, trg_shape=None,
                         grib_filters=[{'shortName':'t',
                                        'dataType':['an', 'fc']}],
                         concat_dim='step', sel_dict=None, isel_dict=None,
                         attrs={'Conventions': 'CF-1.5'}, encoding={}):

    logger.info("\tProcessing GRIB file: "
                "{}".format(grib_path))

    src_xdset = open_grib_dataset(grib_path, grib_filters,
                                  concat_dim, sel_dict, isel_dict)

    if src_proj_str is None or src_geot is None:
        (src_proj_str, src_geot,
         src_shape) = get_georef_from_file(grib_path)

    if trg_proj_str is None:
        out_xdset = georef_xdset(src_xdset, src_proj_str, src_geot, src_shape)
    else:
        out_xdset = reproject_dataset(src_xdset, src_proj_str, src_geot,
                                      trg_proj_str, trg_wkt, trg_geot,
                                      trg_shape)

    for xvar in out_xdset.data_vars.values():
        dset_enc = encoding.setdefault(xvar.name, {})
        dset_enc.update({'zlib': True, 'complevel': 9})
        dset_attrs = attrs.pop(xvar.name, {})
        out_xdset.get(xvar.name).attrs.update(dset_attrs)
    out_xdset.attrs.update(attrs)

    out_xdset.to_netcdf(
        netcdf_path, encoding=encoding,
    )
    logger.info("\tWrite netcdf file successfuly: {}".format(netcdf_path))


def open_grib_dataset(grib_path, grib_filters=[{'shortName':'t',
                                                'dataType':['an', 'fc']}],
                      concat_dim='step', sel_dict=None, isel_dict=None):

    xdset_list = _select_dataset_from_grib(grib_path, grib_filters)

    var_names = set()
    for xdset in xdset_list:
        for var_name in xdset.data_vars:
            var_names.add(var_name)

    xvar_list = []
    for var_name in var_names:
        xvars_to_concat = [dset.get(var_name) for dset in xdset_list
                           if var_name in dset.data_vars]
        _expand_dims_to_concat(xvars_to_concat)
        xvar = xr.concat(xvars_to_concat, dim=concat_dim)
        if sel_dict is not None:
            logger.debug("\txarray.DataArray label selection: {}".format(sel_dict))
            xvar = xvar.sel(**sel_dict)
        if isel_dict is not None:
            logger.debug("\txarray.DataArray index "
                         "selection: {}".format(isel_dict))
            xvar = xvar.isel(**isel_dict)
        xvar_list.append(xvar)
    return xr.merge(xvar_list)


def _select_dataset_from_grib(grib_path,
                              grib_filters=[{'shortName':'t',
                                             'dataType':['an', 'fc']}]):
    grib_idx_path = "/tmp/{}.{}.idx".format(
        os.path.basename(grib_path), str(uuid.uuid1())
    )
    xdset_list = []
    for filter_dict in grib_filters:
        keys, values_list = zip(
            *[(key, value if isinstance(value, list) else [value])
              for key, value in filter_dict.items()]
        )
        for single_values in product(*values_list):
            keywords_filter = dict(zip(keys, single_values))
            logger.info("\tFilter GRIB messages "
                        "with: {}".format(keywords_filter))
            xdset_list.append(
                xr.open_dataset(
                    grib_path, engine='cfgrib',
                    backend_kwargs={'filter_by_keys':keywords_filter,
                                    'indexpath':grib_idx_path}
                )
            )
    if os.path.isfile(grib_idx_path):
        os.remove(grib_idx_path)
    return xdset_list


def _expand_dims_to_concat(xvar_list, concat_dim='step',
                           expand_reftime=True): 
    # Sort xvars by number of dims:
    xvar_list.sort(key=lambda xvar: len(xvar.dims), reverse=True)

    if 'time' in xvar_list[0].coords:
        _fix_grib_time_units(xvar_list[0])
        if expand_reftime: 
            xvar_list[0] = xvar_list[0].expand_dims('time')

    if len(xvar_list) > 1:
        for idx in range(1, len(xvar_list)):
            for dim in reversed(xvar_list[0].dims):
                if dim not in xvar_list[idx].dims:
                    xvar_list[idx] = xvar_list[idx].expand_dims(dim)
            if 'time' in xvar_list[idx].coords:
                _fix_grib_time_units(xvar_list[idx])

    xvar_list.sort( 
        key=lambda xvar: (
            xvar.coords.get(concat_dim).values
            if np.isscalar(xvar.coords.get(concat_dim).values)
            else xvar.coords.get(concat_dim).values[0]
        ) 
    )


def _fix_grib_time_units(xvar):
    start_time = (xvar.time.values
                  if np.isscalar(xvar.time.values)
                  else xvar.time.values[0])
    xvar.time.encoding['units'] = 'hours since {}'.format(
        np.datetime_as_string(start_time).replace('T', ' ')
    )


def get_georef_from_file(path):
    gfid = gdal.Open(path, gdal.GA_ReadOnly)
    if (not any(gfid.GetProjection()) and
            any(gfid.GetSubDatasets())):
        gfid = gdal.Open(gfid.GetSubDatasets()[0][0])
    proj_str, geo_t, shape = get_georef_from_subdataset(gfid)
    gfid = None
    return proj_str, geo_t, shape


def get_georef_from_subdataset(gdset):
    proj_str = wkt_to_proj(gdset.GetProjection())
    geo_t = gdset.GetGeoTransform()
    shape = (gdset.RasterYSize, gdset.RasterXSize)
    # TODO: Get geo_transform from xvar to avoid errors
    #       in case of subregion selection or topdown (fliped) bands:

    return proj_str, geo_t, shape


def reproject_dataset(xdset, src_proj_str, src_geot, trg_proj_str=None,
                      trg_wkt=None, trg_geot=None, trg_shape=None):

    reproj_xdset_list = []
    for xvar in xdset.data_vars.values():
        mem_gdset, bands_order_encoder = (
            xvar_to_gdal_subdataset(xvar, src_proj_str, src_geot)
        )
        rep_gdset = reproject_gdal_subdataset(mem_gdset, trg_proj_str,
                                              trg_wkt, trg_geot, trg_shape)
        reproj_xdset_list.append(
            gdal_subdataset_to_xdset(rep_gdset, bands_order_encoder, xvar)
        )

    return xr.merge(reproj_xdset_list)


def xvar_to_gdal_subdataset(xvar, src_proj_str, src_geot):


    logger.info("\txarray.DataArray to GDAL MEM Subdataset...")

    non_horizontal_coords = [xvar.coords.get(dim_name)
                             for dim_name in list(xvar.dims)[:-2]]
    nbands = int(np.prod([coord.size for coord in non_horizontal_coords]).item())
    x_coord = xvar.coords.get(xvar.dims[-1])
    y_coord = xvar.coords.get(xvar.dims[-2])
    enc_xvar = xr.conventions.encode_cf_variable(xvar)
    fill_value = enc_xvar.attrs.get('_FillValue')
    if fill_value is None:
        fill_value = np.nan    
    else:
        fill_value = fill_value.item()
    gdal_type_code = gdal_array.NumericTypeCodeToGDALTypeCode(enc_xvar.dtype)
    mem_subdataset = gdal.GetDriverByName('MEM').Create(
        '', x_coord.size, y_coord.size, nbands, gdal_type_code
    )
    bands_order_encoder = list(
        product(*[range(coord.size) for coord in non_horizontal_coords])
    )
    for band_it, index_selection in enumerate(bands_order_encoder):
        label_selection = dict(
            zip([coord.name for coord in non_horizontal_coords],
                index_selection)
        )
        logger.debug("\tPacking xarray.DataArray (label selection: {}) "
                     "into GDAL raster band ({})"
                     "".format(label_selection, band_it+1))
        mem_band = mem_subdataset.GetRasterBand(band_it+1)
        mem_band.SetNoDataValue(fill_value)
        mem_band.Fill(fill_value)
        mem_band.WriteArray(enc_xvar.isel(**label_selection).values[:,:])

    mem_subdataset.SetProjection(
        proj_to_wkt(src_proj_str)
    )
    mem_subdataset.SetGeoTransform(src_geot)

    return mem_subdataset, bands_order_encoder


def proj_to_wkt(proj_str):
    srs = osr.SpatialReference()
    srs.ImportFromProj4(proj_str)
    return srs.ExportToWkt()


def wkt_to_proj(wkt):
    srs = osr.SpatialReference(wkt)
    return srs.ExportToProj4()


def reproject_gdal_subdataset(src_subdataset, trg_proj_str=None, trg_wkt=None,
                              trg_geo_transform=None, trg_shape=None,
                              error_threshold=0.125,
                              method=gdal.GRA_NearestNeighbour):
    src_wkt = src_subdataset.GetProjection()
    if trg_proj_str is not None:
        logger.info("\tReprojecting GDAL Subdataset"
                    " into PROJ4: '{}'".format(trg_proj_str))
        trg_wkt = proj_to_wkt(trg_proj_str)
    elif trg_wkt is not None:
        logger.info("\tReprojecting GDAL Subdataset"
                    " into WKT: '{}'".format(trg_wkt))

    # It seems that gdal-pytho3 multiple band datasets
    # reprojection isn't working as expected...
    # So the reprojection process takes place band by band.
    for idx in range(1, 1+src_subdataset.RasterCount):
        # Create one band dataset and reproject:
        src_band = src_subdataset.GetRasterBand(idx)
        one_band_src_sdset = gdal.GetDriverByName('MEM').Create(
            '', src_subdataset.RasterXSize,
            src_subdataset.RasterYSize, 1, src_band.DataType
        )
        one_band_src_sdset.SetProjection(src_subdataset.GetProjection())
        one_band_src_sdset.SetGeoTransform(src_subdataset.GetGeoTransform())
        one_band_src_band = one_band_src_sdset.GetRasterBand(1)
        one_band_src_band.SetScale(src_band.GetScale())
        one_band_src_band.SetOffset(src_band.GetOffset())
        one_band_src_band.SetNoDataValue(src_band.GetNoDataValue())
        one_band_src_band.WriteArray(src_band.ReadAsArray())

        # Reproject one band
        if trg_geo_transform is None or trg_shape is None:
            trg_geo_transform, (y_size, x_size) = nc_proj.nc_suggested_warp_output(
                src_subdataset.GetGeoTransform(),
                (src_subdataset.RasterYSize, src_subdataset.RasterXSize),
                 src_wkt, trg_wkt
            )
            trg_shape = (y_size, x_size)

        one_band_trg_sdset = gdal.GetDriverByName('MEM').Create(
            '', trg_shape[1], trg_shape[0], 1, src_band.DataType
        )
        one_band_trg_sdset.SetProjection(trg_wkt)
        one_band_trg_sdset.SetGeoTransform(trg_geo_transform)
        one_band_trg_band = one_band_trg_sdset.GetRasterBand(1)
        one_band_trg_band.SetScale(src_band.GetScale())
        one_band_trg_band.SetOffset(src_band.GetOffset())
        one_band_trg_band.SetNoDataValue(src_band.GetNoDataValue())
        one_band_trg_band.Fill(src_band.GetNoDataValue())
        gdal.ReprojectImage(one_band_src_sdset, one_band_trg_sdset,
                            one_band_src_sdset.GetProjection(),
                            one_band_trg_sdset.GetProjection(),
                            method, maxerror=error_threshold)

        if idx == 1:
            trg_subdataset = gdal.GetDriverByName('MEM').Create(
                '', one_band_trg_sdset.RasterXSize,
                one_band_trg_sdset.RasterYSize,
                src_subdataset.RasterCount, src_band.DataType
            )
            trg_subdataset.SetProjection(one_band_trg_sdset.GetProjection())
            trg_subdataset.SetGeoTransform(one_band_trg_sdset.GetGeoTransform())
        trg_band = trg_subdataset.GetRasterBand(idx)
        one_band_trg_band = one_band_trg_sdset.GetRasterBand(1)
        trg_band.SetScale(one_band_trg_band.GetScale())
        trg_band.SetOffset(one_band_trg_band.GetOffset())
        trg_band.SetNoDataValue(one_band_trg_band.GetNoDataValue())
        data_array = one_band_trg_band.ReadAsArray()
        trg_band.WriteArray(data_array)

    return  trg_subdataset
 

def gdal_subdataset_to_xdset(gdset, bands_order_encoder, src_xvar):

    logger.info("\tGDAL MEM Subdataset to xarray.DataArray...")
    proj_str, geo_t, hshape = get_georef_from_subdataset(gdset)
    
    x_values, y_values = nc_proj.nc_coord_values(
        geo_t, hshape[1], hshape[0]
    )

    y_coord = xr.DataArray(
        np.array(y_values), name='y', dims=('y',)
    )
    x_coord = xr.DataArray(
        np.array(x_values), name='x', dims=('x',)
    )

    non_horizontal_coords = [src_xvar.coords.get(dim)
                             for dim in list(src_xvar.dims)[:-2]]
    coords = OrderedDict() 
    for coord in non_horizontal_coords:
            coords[coord.name] = coord
    for coord in [y_coord, x_coord]:
            coords[coord.name] = coord
    data_shape = [c.size for c in coords.values()]
    var_data = np.ones(data_shape, src_xvar.dtype)
    xvar = xr.DataArray(var_data, dims=coords.keys(), coords=coords)
    for band_it, index_selection in enumerate(bands_order_encoder):
        label_selection = dict(zip([coord.name
                                    for coord in non_horizontal_coords],
                                   index_selection))
        logger.debug("\tPacking GDAL raster band ({}) into "
                     "xarray.DataArray (label selection: {})"
                     "".format(band_it+1, label_selection))
        gdal_band = gdset.GetRasterBand(band_it+1)
        logger.debug("\tRead gdal raster band data...")
        band_data = gdal_band.ReadAsArray()
        logger.debug("\t Write xarray.DataArray...")
        xvar[label_selection] = band_data
    enc_xvar = xr.conventions.encode_cf_variable(src_xvar)
    xvar.encoding.update(enc_xvar.encoding)
    xvar.attrs.update(enc_xvar.attrs)
    return georef_xdset(xr.Dataset({src_xvar.name: xvar}),
                        proj_str, geo_t, hshape)


def georef_xdset(xdset, proj_str, geo_transform, hshape):

    logger.info("\tAdding georeference variables/attributes "
                "to xarray.DataArray...")

    grid_mapping_attrs = nc_proj.nc_grid_mapping_attrs(proj_str)
    xcrs_name = grid_mapping_attrs['grid_mapping_name'].decode()
    xcrs = xr.DataArray('', dims=[],
                        attrs={k:v.decode() if isinstance(v, bytes) else v
                               for k,v in grid_mapping_attrs.items()})
    # Add spatial_ref attribute (redundant...)
    xcrs.attrs['spatial_ref'] = proj_to_wkt(proj_str)
    xdim_name, xdim_attrs = nc_proj.nc_xcoord_attrs(proj_str)
    ydim_name, ydim_attrs = nc_proj.nc_ycoord_attrs(proj_str)
    xdim_data, ydim_data = nc_proj.nc_coord_values(geo_transform,
                                                   hshape[1], hshape[0])

    y_coord = xr.DataArray(ydim_data, name=ydim_name, dims=(ydim_name,),
                           attrs={k:v.decode() for k,v in ydim_attrs.items()})
    x_coord = xr.DataArray(xdim_data, name=xdim_name, dims=(xdim_name,),
                           attrs={k:v.decode() for k,v in xdim_attrs.items()})

    dset_dict = {xcrs_name: xcrs}
    
    for var_name in list(xdset.data_vars):
        xvar = xdset.get(var_name)
        replace_dims_dict = dict(zip(xvar.dims[-2:], [ydim_name, xdim_name]))
        dset_dict[var_name] = xvar.rename(**replace_dims_dict)
        dset_dict[var_name] = dset_dict[var_name].assign_coords(
            **{ydim_name: y_coord, xdim_name: x_coord}
        )
        dset_dict[var_name].attrs['grid_mapping'] = xcrs_name
        # Temporary clean GRIB attributes
        for key in list(dset_dict[var_name].attrs.keys()):
            if key.startswith('GRIB_'):
                dset_dict[var_name].attrs.pop(key)
            
    logger.info("\tCreate georeferenced xarray.Dataset...")
    out_xdset = xr.Dataset(dset_dict)
    out_xdset.attrs.update(xdset.attrs)
    return out_xdset


def extend_time_coord(xdset, start_datetime, end_datetime,
                      timedelta=dt.timedelta(hours=12),
                      coord_name='time'):
    coord_var = xdset.get(coord_name)
    coord_data = np.arange(np.datetime64(start_datetime),
                           np.datetime64(end_datetime),
                           np.timedelta64(timedelta))
    extended_coord = xr.DataArray(
        coord_data, name=coord_name, dims=(coord_name,),
        attrs={k:v for k,v in coord_var.attrs.items()}
    )

    dset_dict = {} 
    for var_name in list(xdset.data_vars):
        enc_xvar = xr.conventions.encode_cf_variable(xdset.get(var_name))
        if coord_name in enc_xvar.dims:
            coords = OrderedDict([
                (dim_name, enc_xvar.coords.get(dim_name)
                 if dim_name != coord_name else extended_coord)
                for dim_name in enc_xvar.dims
            ]) 

            data_shape = [coords.get(dim_name).size
                          for dim_name in enc_xvar.dims]
            var_data = np.ones(data_shape, enc_xvar.dtype)
            fill_value = enc_xvar.attrs.get('_FillValue')
            if fill_value is not None:
                var_data *= fill_value
            xvar = xr.DataArray(var_data, dims=coords.keys(),
                                coords=coords)

            xvar.encoding.update(enc_xvar.encoding)
            xvar.attrs.update(enc_xvar.attrs)
            dset_dict[var_name] = xvar
        else:
            dset_dict[var_name] = enc_xvar

    logger.info("\tCreate extended xarray.Dataset...")
    return xr.Dataset(dset_dict)


def create_empty_dataset(fname, xdset,
                         props_dict={'_all': {'zlib': True,
                                              'complevel': 9}}):
    # Use encoded datasets as input xdset.

    logger.info("\tCreate netCDF file: {}".format(fname))
    nc_fid = Dataset(fname, 'w')
    for coord_name in xdset.coords:
        coord = xr.conventions.encode_cf_variable(xdset.get(coord_name))
        logger.debug("\tCreate netCDF coordinate: {}".format(coord_name))
        nc_fid.createDimension(coord_name, coord.size)
        create_props = {'dimensions': coord.dims}
        fill_value = coord.attrs.get('_FillValue')
        if fill_value is not None:
            create_props['fill_value'] = fill_value
        nc_coord = nc_fid.createVariable(coord_name, coord.dtype,
                                         **create_props)
        nc_fid.sync()
        nc_coord[...] = coord.values
        for attr_name, attr_value in coord.attrs.items():
            if attr_name not in nc_coord.ncattrs():
                logger.debug("\tCreate netCDF coordinate attribute: ({}, {})"
                             "".format(attr_name, attr_value))
                nc_coord.setncattr(attr_name, attr_value)
                nc_fid.sync()

    
    for var_name in xdset.data_vars:
        #xvar = xr.conventions.encode_cf_variable(xdset.get(var_name))
        xvar = xdset.get(var_name)
        create_props = {'dimensions': xvar.dims}
        dtype = xvar.dtype
        fill_value = xvar.attrs.get('_FillValue')
        if fill_value is not None:
            create_props['fill_value'] = fill_value

        for prop in ['zlib', 'complevel',
                     'fill_value', 'dtype']:
            prop_value = reduce_dict(
                props_dict, ['_all', prop],
                reduce_dict(props_dict, [var_name, prop])
            )
            if prop_value is not None:
                if prop == 'dtype':
                    dtype = prop_value
                else:
                    create_props[prop] = prop_value

        logger.debug("\tCreate netCDF variable: {} (dtype {})".format(var_name, dtype))
        nc_var = nc_fid.createVariable(var_name, dtype, **create_props)
        nc_fid.sync()
        for attr_name, attr_value in xvar.attrs.items():
            if attr_name not in nc_var.ncattrs():
                logger.debug("\tCreate netCDF variable attribute: ({}, {})"
                             "".format(attr_name, attr_value))
                nc_var.setncattr(attr_name, attr_value)
                nc_fid.sync()
    return nc_fid


def chunk_processing_prototype(xdset, except_vars=[], n_chunks=(10, 10)):

    for xvar_name in xdset.data_vars:
        if xvar_name not in except_vars:
            xvar = xdset.get(xvar_name)
            row_bnds, col_bnds = list(zip((0,0), (xvar.coords[dim].size
                                                  for dim in xvar.dims[-2:])))
            row_chunk_bnds = np.arange(row_bnds[0], row_bnds[1],
                                       row_bnds[1] / n_chunks[0], dtype='i4')
            col_chunk_bnds = np.arange(col_bnds[0], col_bnds[1],
                                       col_bnds[1] / n_chunks[1], dtype='i4')
            for row_chk_idx, row_chunk_it in enumerate(row_chunk_bnds):
                 start_row = row_chunk_it
                 end_row = (row_chunk_bnds[row_chk_idx + 1]
                            if (row_chk_idx + 1) != row_chunk_bnds.size
                            else None)
                 for col_chk_idx, col_chunk_it in enumerate(col_chunk_bnds):
                     start_col = col_chunk_it
                     end_col = (col_chunk_bnds[col_chk_idx + 1]
                                if (col_chk_idx + 1) != col_chunk_bnds.size
                                else None)
                     print("row_bnds: [{}, {}]; col_bnds: [{}, {}]"
                           "".format(start_row, end_row, start_col, end_col))
   

def reduce_dict(in_dict, keys_list, default=None):
    return reduce(lambda d, k: d.get(k) if isinstance(d, dict)
                  else default, keys_list, in_dict)


def copy_to_disk(nc_fid, xvar, n_chunks=(1,1)):
    row_bnds, col_bnds = list(zip((0,0), xvar.shape))

    row_chunk_bnds = np.arange(
        row_bnds[0], row_bnds[1], row_bnds[1] / n_chunks[0], dtype='i4'
    )

    col_chunk_bnds = np.arange(
        col_bnds[0], col_bnds[1], col_bnds[1] / n_chunks[1], dtype='i4'
    )

    for row_chk_idx, row_chunk_it in enumerate(row_chunk_bnds):
        for col_chk_idx, col_chunk_it in enumerate(col_chunk_bnds):
            start_row = row_chunk_it
            end_row = (row_chunk_bnds[row_chk_idx + 1]
                       if (row_chk_idx + 1) != row_chunk_bnds.size else None)
            start_col = col_chunk_it
            end_col = (col_chunk_bnds[col_chk_idx + 1]
                       if (col_chk_idx + 1) != col_chunk_bnds.size else None)
            print("row_bnds: [{}, {}]; col_bnds: [{}, {}]"
                  "".format(start_row, end_row, start_col, end_col))
            nc_fid[xvar.name][start_row: end_row,
                              start_col: end_col] = xvar[start_row: end_row,
                                                         start_col: end_col]
            nc_fid.sync()


def get_available_values(grib_path,
                         keywords_list=['shortName', 'dataType',
                                        'step', 'level',
                                        'numberOfPoints']):
    """
    Auxiliary function to dump available keywords values
    from a given GRIB file path and keywords_list.
    """
    gfid = FileStream(grib_path)
    keyword_values = {}
    for message in gfid:
        for keyword in keywords_list:
            try:
                value = message.get(keyword)
                values = keyword_values.setdefault(keyword, set())
                values.add(tuple(value) if isinstance(value, list) else value)
            except TypeError:
                pass
    return keyword_values


def get_available_keywords(grib_path):
    """
    Auxiliary function to dump available keywords from a given
    GRIB file path.
    """
    gfid = FileStream(grib_path)
    keywords = set()
    for message in gfid:
        for keyword in message.keys():
            keywords.add(keyword)
    return keywords


