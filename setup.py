#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
setup.py file for SWIG example
From http://www.swig.org/Doc3.0/Python.html#Python_importfrominit
"""

from distutils.core import setup, Extension

nc_proj_module = Extension(
    'nc_juice._nc_proj',
    include_dirs=[
        'src/cpp',
        'src/cpp/netcdf'
    ],
    libraries=['gdal'],
    sources=[
        'src/cpp/nc_proj_wrap.cpp',
        'src/cpp/nc_proj.cpp',
        'src/cpp/pync_proj.cpp',
        'src/cpp/netcdf/gmtdataset.cpp',
        'src/cpp/netcdf/netcdfwriterconfig.cpp',
        'src/cpp/netcdf/netcdflayer.cpp',
        'src/cpp/netcdf/netcdfdataset.cpp'
    ],
    extra_compile_args=['-std=c++11'],
    language='c++11',
)

setup(
    name='nc_juice',
    version='0.1.2',
    author="João Macedo",
    description="NetCDF files manager based on xarray and python-gdal.",
    ext_modules=[nc_proj_module],
    packages=['nc_juice'],
    package_dir={"": "src/python"},
    install_requires=[
        "cfgrib",
        "netcdf4",
        "numpy",
        "xarray",
    ],
)
